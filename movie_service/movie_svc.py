from flask import Flask, jsonify
import random
import socket

app = Flask(__name__)

@app.route('/')
def welcome():
    return 'This is Movie Service!<br>' + 'Hostname: ' + str(socket.gethostname())

@app.route('/movie-list')
def get_movie_list():
    movie_lists = ['Spiderman', 'The Warriors', 'Ultraman', 'Avengers']
    movie_desc_lists = ['Film Spiderman', 'Film The Warriors', 'Film Ultraman', 'Film Avengers']
    lists_return = []
    total_movies = len(movie_lists)
    r = random.sample(range(1, total_movies+1), k=total_movies)
    for i in range(total_movies):
        lists_return.append(
            {
            movie_lists[i]:movie_desc_lists[i], 
            'studio': r[i]
            }
        )
    json_data = jsonify(lists_return)
    return json_data

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')