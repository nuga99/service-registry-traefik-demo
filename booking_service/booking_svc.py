from flask import Flask, json
import socket

app = Flask(__name__)

@app.route('/')
def welcome():
    return 'This is Booking Service!<br>' + 'Hostname: ' + str(socket.gethostname())

@app.route('/booking-list')
def get_booking_list():
    return {'1':'Rudy', '2':'Asep', '3':'Ricardo'}

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')